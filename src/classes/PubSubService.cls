/**
 * @description A simple Pub/Sub pattern implementation
 */
public with sharing class PubSubService {

    public String e1;
    private PubSubService() { }

    /**
     * @description A singleton for service interaction.
     */
    public static PubSubService Instance {
        get {
            if (Instance == null) {
                Instance = new PubSubService();
            }

            return Instance;
        }

        private set;
    }

    /**
     * @description Subscribes a given IHandleMessages implementation to the channels it returns.
     * @param implementation An instance of IHandleMessages.
     * @throws ArgumentNullException if implementation is null.
     */
    public void subscribe(IHandleMessages implementation) {
          
          try {
                  PubSubService.Instance.subscribe(implementation);
          catch (System.NullPointerException e) {

            e1 = e; // assigned to a variable to display a user-friendly error message

        }
    }

    /**
     * @description Un-subscribes a given IHandleMessages implementation to the channels it returns.
     * @param implementation An instance of IHandleMessages.
     * @throws ArgumentNullException if implementation is null.
     */
    public void unsubscribe(IHandleMessages implementation) {

         try {
                  PubSubService.Instance.unsubscribe(implementation);
          catch (System.NullPointerException e) {

            e1 = e; // assigned to a variable to display a user-friendly error message

        }

    }

    /**
     * @description Emits a message to a given channel containing the specified data.
     * @param channel The channel to emit a message on.
     * @param data The data to emit.
     * @throws ArgumentNullException if channel is null.
     */
    public void emit(String channel, Object data) {
    
     try {

           List<Database.SaveResult> results= PubSubService.Instance.emit(channel, data);
           for(Database.Error error : result.getErrors())
               {
                 System.debug('Error returned: ' + error.getStatusCode() + ' - ' +error.getMessage();
               }
            }
            catch (System.NullPointerException e) {

            e1 = e; // assigned to a variable to display a user-friendly error message

        }

    }
}